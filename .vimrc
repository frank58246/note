"Basic setting
inoremap jj <Esc>
set number
syntax on
set showmode
set showcmd
set t_Co=256
filetype indent on

"indent
set autoindent
set tabstop=2
set shiftwidth=4
set expandtab "change tab to space
set softtabstop=2

"appearance
set number
set relativenumber
set textwidth=80
set linebreak 
set scrolloff=5
set laststatus=2
set ruler

"search
set showmatch 
set hlsearch
set incsearch
set ignorecase
set smartcase

"edit
set spell spelllang=en_us
set nobackup
set noswapfile
set noerrorbells
set visualbell
set autoread
set wildmenu
set wildmode=longest:list,full
