import Vue from 'vue'
import VueRouter from 'vue-router'
import Note from '../components/Note2'
import Vim from '../components/Vim'
import JQuery from '../components/JQuery'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Note',
    component: Note
  },
  {
    path: '/vim',
    name: 'Vim',
    component: Vim
  },
  {
    path: '/jquery',
    name: 'JQuery',
    component: JQuery
  }

]

const router = new VueRouter({
  routes
})

export default router
